# App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

# What is achieved:

1. Implemented two modules _dashboard_ and _users_ (package _pages_) which use one single component **Grid** in  _shared_ module
In the same time grid is composition of another component names **Card**

2. Added Responsive layout for card layout. Depends on screen resolution we show 5 /  4  / 2 item in grid's row

3. Access to page by two level of permission:
  * is logged in user (authorization criteria is successfully http request on session user, which is stubbed file). **AuthGuard**
  * user`s role and action permission. **PermissionGuard**

 
* _/dashboard_ we allow to navigate for all authorized users.

* _/users_ we allow to navigate **only** for admin user with appropriate action permission.
  
Just play with src/assets/sessionUser.json
  
4. Demonstrate experience with material component, at least with 
    Button
    Input
    Sidenav,
    Icon,
    Toolbar,
    SnackBar,
    Card,
    ButtonToggle
    
5. Implement Ordering / Filtering operation.
Handle changes of multiple control in single place in Rx way

6. deployed on firebase hosting: https://rich-digital-ocean.firebaseapp.com

Prod build with ahead of time compilation and tree shaking. Total project size: 152kb


