import { Component, OnInit } from '@angular/core';
import { Tile } from '../../model/tile';
import { DashboardService } from '../../providers/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dataSource: Tile[];

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.service.fetchList()
      .subscribe(items => this.dataSource = items.reverse());
  }
}
