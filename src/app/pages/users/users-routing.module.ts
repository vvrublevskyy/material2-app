import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { PermissionGuard } from '../../providers/guards/permission.guard';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [PermissionGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PermissionGuard]
})
export class UsersRoutingModule {
}
