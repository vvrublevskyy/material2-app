import { Component, OnInit } from '@angular/core';
import { Tile } from '../../model/tile';
import { DashboardService } from '../../providers/dashboard.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  dataSource: Tile[];

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.service.fetchList()
      .subscribe(items => this.dataSource = items);
  }
}
