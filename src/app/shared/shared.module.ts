import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatGridListModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatSidenavModule, MatSnackBarModule, MatToolbarModule
} from '@angular/material';
import { GridComponent } from './component/grid/grid.component';
import { CardComponent } from './component/card/card.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatInputModule,
    MatGridListModule,
    MatSidenavModule,
    MatNativeDateModule,
    MatIconModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatCardModule,
    MatButtonToggleModule
  ],
  declarations: [
    GridComponent,
    CardComponent
  ],
  exports: [
    GridComponent,
    CardComponent,

    /* Imports all of the Angular Material components that we will use in our application */
    MatButtonModule,
    MatDatepickerModule,
    MatInputModule,
    MatGridListModule,
    MatSidenavModule,
    MatNativeDateModule,
    MatIconModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatCardModule,
    MatButtonToggleModule
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders  {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
