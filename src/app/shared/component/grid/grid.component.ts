import {
  AfterViewChecked,
  ChangeDetectorRef, Component, ElementRef, Input, OnInit, QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { Tile } from '../../../model/tile';
import { CardComponent } from '../card/card.component';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { MatButtonToggleGroup } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'grid-component',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, AfterViewChecked {
  /**
   * Whole data source
   * */
  @Input('dataSource')
  dataSource: Tile[];

  /**
   * Matched items. Filtering & Sorting
   * Binded in templete
   * */
  dataSource$: Observable<Tile[]>;

  _orderChange: BehaviorSubject<string> = new BehaviorSubject('');
  get order(): string { return this._orderChange.value; }
  set order(filter: string) { this._orderChange.next(filter); }

  @Input()
  enableActions: boolean;

  layout: string;

  @ViewChild('filter') filter: ElementRef;

  @ViewChildren(CardComponent) tiles: QueryList<CardComponent>;

  constructor(private _cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.layout = 'dashboard';

    // Rx based approach to handle both controls: sorting and filtering
    const displayDataChanges = [
      Observable.fromEvent(this.filter.nativeElement, 'keyup'),
      this._orderChange
    ];

    this.dataSource$ = Observable.merge(...displayDataChanges)
      .debounceTime(150)
      .distinctUntilChanged()
      .map(() => this.applyFilterAndSorting());
  }


  applyFilterAndSorting(): Tile[] {
    const query: string = this.filter.nativeElement.value;
    let results = this.dataSource;
    // Filtering
    if (query.trim().length > 0) {
      results = this.dataSource.filter(item => item.contains(query));
    }
    // Ordering
    if (this.order) {
      results = _.orderBy(results, 'title', this.order);
    }
    return results;
  }

  ngAfterViewChecked() {
    this.tiles.forEach((component: CardComponent) => component.hasActions = this.enableActions);
    this._cdr.detectChanges();
  }

}
