import { Component, Input, OnInit } from '@angular/core';
import { Tile } from '../../../model/tile';

@Component({
  selector: 'card-component',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input()
  tile: Tile;

  @Input()
  hasActions: boolean;

  constructor() { }

  ngOnInit() {
  }

}
