export class Tile {
  id: string;
  title: string;
  avatar: string;
  link: string;

  constructor(data?: any) {
    this.link = 'diabetes.uk';
    if (data) {
      this.id = data.id;
      this.title = data.title;
      this.avatar = data.avatar;
    }
  }

  contains(filterString: string) {
    return this.title.toUpperCase().indexOf(filterString.toUpperCase()) !== -1;
  }
}
