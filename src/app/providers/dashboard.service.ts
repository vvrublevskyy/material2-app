import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Tile } from '../model/tile';

@Injectable()
export class DashboardService {
  constructor(private http: Http) {
  }

  fetchList(): Observable<Tile[]> {
    return this.http.get('assets/tileList.json')
      .map((response: Response) => response.json().data)
      .map((items: any[]) => items.map(item => new Tile(item)));
  }

}
