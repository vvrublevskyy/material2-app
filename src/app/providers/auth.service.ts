import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { SessionUser } from '../model/session-user';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Role } from '../enum/role.enum';

/**
 * Global service to handle user
 * */

@Injectable()
export class AuthService {
  currentUser: SessionUser;

  constructor(private http: Http,
              private router: Router,
              protected toaster: MatSnackBar) {
  }

  /*
  * Fetch session user info once.
  * Prevent redundant http calls
  * */
  isLoggedIn(): Observable<boolean> {
    if (this.currentUser) {
      return Observable.of(true);
    }

    return  this.http.get('assets/sessionUser.json')
      .map((resp: Response) => resp.json().user)
      .map(data => this.currentUser = new SessionUser(data))
      .map((user: SessionUser) => !!user)
      .catch(err => {
        this.toaster.open('Unauthorized User', 'Ok' , {duration: 2000});
        return Observable.throw(err);
      });
  }

  hasRole(role: Role): boolean {
    return this.currentUser && this.currentUser.role === role;
  }

  hasPermission(...actions: string[]): boolean {
    return this.currentUser && this.currentUser.hasPermission(actions);
  }
}
